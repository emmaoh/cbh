const { deterministicPartitionKey, 
        deterministicCandidate } = require("./dpk");
const crypto = require("crypto");

describe("deterministicPartitionKey", () => {
  it("It returns candidate that is value of partitionKey when given input event that is an object and value of partitionKey is a string less than 256 characters", () => {
    const event = {partitionKey: "candidate1"}
    const candidate = deterministicPartitionKey(event);
    expect(candidate).toBe("candidate1");
  });

  it("It returns candidate that is encrypted with crypto for value of partitionKey when given input event that is an object and value of partitionKey is a string greater than 256 characters", () => {
    const event = {partitionKey: "candidate1candidate1candidate1candidate1candidate1candidate1candidate1candidate1candidate1candidate1candidate1candidate1candidate1candidate1candidate1candidate1candidate1candidate1candidate1candidate1candidate1candidate1candidate1candidate1candidate1candidate1candidate1candidate1"}
    const candidate = deterministicCandidate(event);
    expect(candidate).toBe(crypto.createHash("sha3-512").update(event.partitionKey).digest("hex"));
  });

  it("It returns candidate that is JSON stringified value of partitionKey when given input event that is an object and value of partitionKey is not string", () => {
    const event = {partitionKey: 11111}
    const candidate = deterministicCandidate(event);
    expect(candidate).toBe(JSON.stringify(11111));
  });

  it("it returns candidate that is encrypted with crypto for input of event that is object but does not have a key, partitionKey", () => {
    const event = {testKey: 11111}
    const candidate = deterministicPartitionKey(event);
    expect(candidate).toBe(crypto.createHash("sha3-512").update(JSON.stringify({testKey: 11111})).digest("hex"));
  });

  it("it returns candidate that is encrypted with crypto for input of event that is not an object", () => {
    const event = 'testKey'
    const candidate = deterministicPartitionKey(event);
    expect(candidate).toBe(crypto.createHash("sha3-512").update(JSON.stringify('testKey')).digest("hex"));
  });

  it("Returns the literal '0' when given no input", () => {
    const trivialKey = deterministicCandidate();
    expect(trivialKey).toBe("0");
  });
});

